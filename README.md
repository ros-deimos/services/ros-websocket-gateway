# ROS Websocket Gateway

ROS Websocket Gateway is a Typescript application who make a bidirectionnal gateway between a ROS topic and a websocket. It is implement as a microservice under Docker image format.


This gateway, implemented in Typescipt, subscribe on `ROS` topics and forward received data on websockets and listen on websocket event and forward received data to a `ROS` topic.

## Usage

```yaml
version: '2'

services:

  ros-master:
    container_name: ros-master
    image: ros:kinetic-ros-core
    environment:
      ROS_HOSTNAME: ros-master
      ROS_MASTER_URI: http://ros-master:11311
    command: stdbuf -o L roscore
    ports:
      - 11311:11311
    networks:
      - ros
    restart: always

  webapp:
    container_name: deimos_dashboard
    restart: always
    build: deimos-dashboard
    image: deimos:dashboard
    environment:
      WEBSOCKET_CAM_ENDPOINT: http://localhost:3000
      WEBSOCKET_SPEED_ENDPOINT: http://localhost:3002
    ports:
      - 9090:80
    networks:
      - ros
    depends_on:
      - ros-master

  video_gateway:
    container_name: video_gateway
    restart: always
    image: registry.gitlab.com/ros-deimos/services/ros-websocket-gateway:dev-stable
    environment:
      ROS_HOSTNAME: video_gateway
      ROS_MASTER_URI: http://ros-master:11311
      ROS_TOPIC: /usb_cam/image_raw/compressed
      ROS_MESSAGE_TYPE: sensor_msgs/CompressedImage
      DEBUG: "true"
    ports:
      - 3000:3000
    networks:
      - ros
    depends_on:
      - ros-master

  speed_gateway:
    container_name: speed_gateway
    restart: always
    image: registry.gitlab.com/ros-deimos/services/ros-websocket-gateway:dev-stable
    environment:
      ROS_HOSTNAME: speed_gateway
      ROS_MASTER_URI: http://ros-master:11311
      ROS_TOPIC: /pwm/speed
      ROS_MESSAGE_TYPE: std_msgs/Int16
    ports:
      - 3002:3000
    networks:
      - ros
    depends_on:
      - ros-master

networks:
  ros:
    driver: bridge

```

## Javascript client exemple

Load socket.io-client in your web page:

```html
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
```

Connect your browser to the websocket:
```javascript

const socketVideo = io(__env.WEBSOCKET_CAM_ENDPOINT);
socketVideo.on('ros:data', (msg) => {
    console.log("message: ", msg)
    $body.css('background-image', `url( data:image/jpeg;base64,${msg.data} )`);
})

const socketSpeed = io(__env.WEBSOCKET_SPEED_ENDPOINT);
socketSpeed.on('ros:data', (msg) => {
    console.log("message: ", msg)
})

```

## License
[MIT](https://choosealicense.com/licenses/mit/)