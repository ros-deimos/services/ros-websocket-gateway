const __env = {

    ROS_HOSTNAME: process.env.ROS_HOSTNAME,
    ROS_MASTER_URI: process.env.ROS_MASTER_URI,
    ROS_NODE: process.env.ROS_NODE,
    ROS_TOPIC: process.env.ROS_TOPIC,
    ROS_MESSAGE_TYPE: process.env.ROS_MESSAGE_TYPE,
    WEBSOCKET_PORT: process.env.WEBSOCKET_PORT="3000",
    DEBUG: process.env.DEBUG,

}

export default __env
