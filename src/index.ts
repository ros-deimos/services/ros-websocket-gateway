

import __env from "./env";
import * as winston from 'winston'
import * as path from 'path'
import * as express from 'express'
import * as io from 'socket.io'
import * as http from 'http'
import * as cv from 'opencv4nodejs'
import * as rosnodejs from 'rosnodejs'

/**
 * Logger configuration
 */
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.Console(),
  ]
});


/**
 * Main process
 */
main()
async function main() {
  
  const app : express.Application = express()
  const server = http.createServer(app)
  const sensorMsgs = rosnodejs.require('sensor_msgs');
  const imageMsg = new sensorMsgs.msg.Image();

  const socketio = io(server, {
    transports:	['websocket'],
    cookie: false
  })

  try {

    /**
     * Connect to master node
     */
    let nh = await rosnodejs.initNode(`/${__env.ROS_NODE}`, { 
      rosMasterUri: __env.ROS_MASTER_URI,
      onTheFly: true
    })
    logger.info("ROS Node initialized !")

    await rosnodejs.loadAllPackages();
    logger.info("ROS all packages loaded !")

    socketio.on('connection', ( client ) => {  
      logger.info(`Web client connected: id: ${client.id}, ip: ${client.handshake.address}`) 
      /**
       * Transfert Websocket to ROS
       */
      let pub = nh.advertise(`/websocket${__env.ROS_TOPIC}`, __env.ROS_MESSAGE_TYPE);
      client.on('ros:data', (msg) => {
        pub.publish({ data: msg })
        if(__env.DEBUG) 
          logger.info(`Emit to ROS ! type ${__env.ROS_MESSAGE_TYPE}, data: ${msg}`) 
      })
    });

    try {
      /**
       * Transfert ROS to Websocket
       */
      logger.info(`ROS topic "${__env.ROS_TOPIC}" subscription initialized ! (type: "${__env.ROS_MESSAGE_TYPE}")`) 
      switch(__env.ROS_MESSAGE_TYPE) {
  
        case 'sensor_msgs/CompressedImage':
  
          nh.subscribe(__env.ROS_TOPIC, __env.ROS_MESSAGE_TYPE, async (message)  => { //receive image 
            try {
              let binData     = new Uint8Array(message.data); 
              let liveImage   = cv.imdecode(binData)
              let outBase64 = cv.imencode('.jpg', liveImage).toString('base64');
              socketio.emit('ros:data', { type: __env.ROS_MESSAGE_TYPE, data: outBase64 })

              if(__env.DEBUG) 
                logger.info(`Emit to websocket ! type ${__env.ROS_MESSAGE_TYPE}, data: ${outBase64}`) 

            } catch(e) {
              logger.error('Error on receive data: ', e)
            }
          }, { queueSize: 1 })
  
        break;
    
        case 'std_msgs/String':
        case 'std_msgs/Int16':

          nh.subscribe(__env.ROS_TOPIC, __env.ROS_MESSAGE_TYPE, async (message)  => { //receive data 
            try {
              socketio.emit('ros:data', { type: __env.ROS_MESSAGE_TYPE, data: message.data })
              if(__env.DEBUG) 
                logger.info(`Emit to websocket ! type ${__env.ROS_MESSAGE_TYPE}, data: ${message.data}`) 
            } catch(e) {
              logger.error('Error on receive data: ', e)
            }
          }, { queueSize: 1 });
  
        break;
      }     
    } catch (e) {
      logger.error("Error: ", e)
    }
  } catch(e) {
    logger.error("Error: ", e)
  }

  
  
  
  server.listen(__env.WEBSOCKET_PORT, function(){
    logger.info(`Gateway is listening on *:${__env.WEBSOCKET_PORT}`);
  });
}

