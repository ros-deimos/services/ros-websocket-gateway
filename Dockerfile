FROM registry.gitlab.com/ros-deimos/docker-images/rcvnode:1.0

# Add project files
ADD . .

# Build project
RUN npm install && npm run build
    
CMD ["npm", "start"]

EXPOSE 3000
